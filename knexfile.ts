import path from 'path';

module.exports = {
  // client: 'mysql',
  // version: '5.7',
  client: 'sqlite3',
  connection: {
    // host: '172.17.0.2',
    // user: 'root',
    // password: '123456',
    // database: 'bth_data'
    filename: path.resolve(__dirname, 'src', 'database', 'database.sqlite')
  },
  pool: {
    min: 2,
    max: 30,
  },
  migrations: {
    directory: path.resolve(__dirname, 'src', 'database', 'migrations')
  },
  seeds: {
    directory: path.resolve(__dirname, 'src', 'database', 'seeds')
  },
  useNullAsDefault: true,
};