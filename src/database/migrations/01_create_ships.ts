import Knex from 'knex';

/**
* @hidden
**/
export async function up(knex: Knex){
    return knex.schema.createTable('ships', table => {
        table.increments('id').primary();
        table.string('name_ship').notNullable();
        table.string('flag').notNullable();

        //TODO Pode ser um usuario?
        table.string('owner').notNullable();
    });
}

/**
* @hidden
**/
export async function down(knex: Knex){
    return knex.schema.dropTable('ships');
}