import Knex from 'knex';

/**
* @hidden
**/
export async function up(knex: Knex){
    return knex.schema.createTable('forgot_passwords', table => {
      table.increments('id').primary();

      table.string('token').nullable();

      table.integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');

      // table.integer('user_id')
      //   .notNullable()
      //   .references('id')
      //   .inTable('users')
      //   .onUpdate('CASCADE')
      //   .onDelete('CASCADE');

      table.boolean('was_used').defaultTo(false);

      table.timestamp('created_at').defaultTo(knex.fn.now());

      // https://stackoverflow.com/questions/57632469/how-to-set-knex-date-to-30-days-in-the-future
      // table.timestamp('expired_at').defaultTo(knex.raw('date_add(?, INTERVAL ? day)', [knex.fn.now(), 1]));
      
    });
}

/**
* @hidden
**/
export async function down(knex: Knex){
    return knex.schema.dropTable('forgot_passwords');
}