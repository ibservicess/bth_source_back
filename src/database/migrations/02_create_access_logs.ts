import Knex from 'knex';

/**
* @hidden
**/
export async function up(knex: Knex){
    return knex.schema.createTable('access_logs', table => {
      table.increments('id').primary();

      table.integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');

      // table.integer('user_id')
      //   .notNullable()
      //   .references('id')
      //   .inTable('users')
      //   .onUpdate('CASCADE')
      //   .onDelete('CASCADE');

      table.timestamp('logged_at').defaultTo(knex.fn.now());
    });
}

/**
* @hidden
**/
export async function down(knex: Knex){
    return knex.schema.dropTable('access_logs');
}