import Knex from 'knex';

/**
* @hidden
**/
export async function up(knex: Knex){
    return knex.schema.createTable('users', table => {
      table.increments('id').unique().primary();

      table.string('name').notNullable();
      table.string('last_name').notNullable();
      table.string('company_name').notNullable();
      table.string('username').unique().notNullable();

      table.string('cnpj').notNullable();
      table.string('email').notNullable();

      table.string('zip_code').notNullable();
      table.string('address_name').notNullable();
      table.string('address_number').notNullable();

      table.integer('level').defaultTo(0).notNullable();

      table.string('password').notNullable();

      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
}

/**
* @hidden
**/
export async function down(knex: Knex){
    return knex.schema.dropTable('users');
}