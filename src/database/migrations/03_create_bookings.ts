import Knex from 'knex';

/**
* @hidden
**/
export async function up(knex: Knex){
    return knex.schema.createTable('bookings', table => {
      table.increments('id').primary();

      // FROM
      table.string('from').notNullable();
      // TO
      table.string('to').notNullable();
      // HAULAGE
      table.integer('haulage').notNullable();
      // EQUIPAMENT
      table.integer('equipament').notNullable();

      table.integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');

      // table.integer('user_id')
      //   .notNullable()
      //   .references('id')
      //   .inTable('users')
      //   .onUpdate('CASCADE')
      //   .onDelete('CASCADE');

      table.timestamp('created_at').defaultTo(knex.fn.now());
    });
}

/**
* @hidden
**/
export async function down(knex: Knex){
    return knex.schema.dropTable('bookings');
}