import knex from 'knex';
import path from 'path';

/**
 * Connection from Database, see docs from [Knex](http://knexjs.org).
 * @param client Type of database.
 * @param filename Path from database
 */
const database = knex({
    client: 'sqlite3',
    connection: {
        filename: path.resolve(__dirname,'database.sqlite')
    },
    useNullAsDefault: true,
});

export default database;