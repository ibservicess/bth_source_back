import Knex from "knex";

/**
* @hidden
**/
export async function seed(knex: Knex): Promise<void> {
    await knex("users").del();

    await knex("users").insert([
        { 
          id: 1, 
          name: "Matheus", 
          last_name: "Gonçalves", 
          company_name: "BTH", 
          username: "matheus_bth", 
          cnpj: "96605020000165", 
          email:"matheusgt4@hotmail.com", 
          zip_code: "11704800",
          address_name: "Rua 1º de Janeiro - Nova Mirim",
          address_number: "488",
          level: 1,
          password: "$2b$14$n2habPFQjVlocEmuRC2YVu99PAQvOzi3LBoFYtOg1fr1InENBOTJW" // bthadmin
        },
        { 
          id: 2, 
          name: "Marcio", 
          last_name: "Novaes", 
          company_name: "BTH", 
          username: "marcio_bth", 
          cnpj: "96605020000165", 
          email:"marciosn@gmail.com", 
          zip_code: "11111111",
          address_name: "Rua -",
          address_number: "488",
          level: 1,
          password: "$2b$14$n2habPFQjVlocEmuRC2YVu99PAQvOzi3LBoFYtOg1fr1InENBOTJW"
        }
    ]);
};
