/**
 * If an error occurs this handler is activated
 * @param error 
 * @param request 
 * @param response 
 * @param next 
 * @category Handlers
 */
export const errorHandler = (error, request, response, next) => {
  response.status(error.status || 500);
  response.json( { error: error.message } );
}