import Mail from "nodemailer/lib/mailer";
import { transporter } from "../service/emailer";

/**
 * Send an email with username.
 * @param to Email to send.
 * @param username Username from user.
 * @param name Name from user.
 * @category Handlers
 */
export const sendUsername = (to: string, username: string, name?: string): void => {
  try {
    let message: Mail.Options = {
      from: {
        name: 'BTH Services',
        address: 'noreply@bth.com'
      },
      to: {
        name: name || '',
        address: to
      },
      subject: 'Welcome to BTH!',
      text: `Welcome to BTH! This is your username ${username}`,
      html: `<h2>Welcome to BTH!</h2>
      <p>This is your username: <br/><h3><strong>${username}</strong></p></h3>`
    };

    transporter.sendMail(message, (error, info) => {
      if (error) {
        console.log(error);
      }
    });
  } catch (error) {
    console.log(error);
  }
}

/**
 * Send an email for reset password to user.
 * @param to Email to send.
 * @param username Username from user.
 * @param token Token for reset password and concat with url.
 * @param name Name for add in `to`.
 * @category Handlers
 */
export const sendResetPassword = (to: string, username: string, token: string, name?: string): void => {
  try {
    let message: Mail.Options = {
      from: {
        name: 'BTH Services',
        address: 'noreply@bth.com'
      },
      to: {
        name: name || '',
        address: to
      },
      subject: 'Reset your password',
      text: `You solicit a reset password 
      your username is ${username} and your token is ${token}
      for reset click this link ${process.env.DOMAIN_URL}/forgot/token?tkn=${token}
      `,
      html: `<h2>Reset password</h2>
      <p>You solicit a rest password:</p>
      <p>This is your username: <strong>${username}</strong></p>
      <p>This is your token: <strong>${token}</strong></p>
      <p>You can reset your password with <a href="${process.env.DOMAIN_URL}/forgot/token?tkn=${token}">this link</a></p>
      <br/>
      <p>If you not solicited this reset password, ignore this email.</p>
      `
    };

    transporter.sendMail(message, (error, info) => {
      if (error) {
        console.log(error);
      }
    });
  } catch (error) {
    console.log(error);
  }
}