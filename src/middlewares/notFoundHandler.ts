import { Request, Response, NextFunction } from "express";

/**
 * If an endpoint is not found this handler is actived.
 * @param request 
 * @param response 
 * @param next 
 * @category Handlers
 */
export const notFoundHandler = (request: Request, response: Response, next: NextFunction) => {
  const message = {
    message: "Endpoint not found"
  };

  response.status(404).json(message);
};