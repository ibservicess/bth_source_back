import { Request, Response, NextFunction } from "express";
import database from "../database/connection";

/**
 * Is activated if an user login.
 * @param request 
 * @param response 
 * @category Handlers
 */
export const logHandler = async (request: Request, response: Response) => {
  const trx = await database.transaction();
  try {

    const { username } = request.body;
    const query = await trx('users').where({ username });
    
    await trx('access_logs').insert({
      user_id: query[0]['id']
    });

    await trx.commit();
  } catch {
    await trx.rollback();

    response.status(401).json({
      error: 'Invalid request'
    });
  }
};