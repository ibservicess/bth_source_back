import { Request, Response, NextFunction } from "express";
import { verifyJWT } from '../utils/jwtUtils';

/**
 * Get a cookie from request's header and verify JWT is valid.
 * @param request For get cookie.
 * @param response If need a response.
 * @param next For next handler or controller.
 * @category Handlers
 */
export const authHandler = (request: Request, response: Response, next: NextFunction) => {
  try {
    const token = request.cookies.auth;
    const decodedToken = verifyJWT(token);
    
    next();
  } catch {
    response.status(401).json({
      error: 'Invalid request'
    });
  }
};