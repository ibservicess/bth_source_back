import { Request, Response } from 'express';

import { bookCreate, bookIndex } from '../repository/BookRepository';

/**
 * Controller for Books
 * @category Controllers
 */
export default class BookController {
  /**
   * Get a list of books.
   * `id` for search specific book.
   * `page` for pagination.
   * @param request 
   * @param response 
   * @category GET
   */
  async index(request: Request, response: Response) {
    try {
      const { id, page = 1 } = request.query;

      const results = await bookIndex(Number(page), Number(id));

      response.header('X-Total-Count', String(results.countFixed));
      response.header('X-Total-Pages', String(results.pages));

      return response.json(results.data);
    } catch (error) {
      return response.status(400).json({
        error: "Unexpected error while searching for books"
      });
    }
  }

  /**
   * Create a book.
   * @param request 
   * @param response 
   * @category POST
   */
  async create(request: Request, response: Response) {
    const {
      user_id,
      from,
      to,
      haulage,
      equipament
    } = request.body;

    try {
      const bookData = {
        user_id,
        from,
        to,
        haulage,
        equipament
      }

      await bookCreate(bookData);

      return response.status(201).json();
    } catch {
      return response.status(400).json({
        error: "Unexpected error while creating new user"
      });

    }

  }

  async update(request: Request, response: Response) {
    return response.status(200).json({ message: "updated!" });
  }

  async delete(request: Request, response: Response) {
    return response.status(200).json({ message: "deleted!" });
  }
}