import { Request, Response, NextFunction } from 'express';
import { transporter } from '../service/emailer';

export default class TestController{
  async index(request: Request, response: Response){
    return response.status(200).json({ message: "listed!" });
  }

  async create(request: Request, response: Response, next: NextFunction){
    return response.status(200).json({ message: "created!" });
  }

  async email(request: Request, response: Response, next: NextFunction){  
    try {
      var message = {
        from: 'noreply@domain.com',
        to: 'matheusgt4@hotmail.com',
        subject: 'Simple email test',
        text: 'This is plain text',
        html: '<p>This is <strong>HTML</strong></p>'
      };

      transporter.sendMail(message, (error, info) => {
        if(error){
          response.status(500).json(error);
        }

        if(info){
          response.status(400).json(info);
        }
      });
      
      return response.status(200).json({ message: "send!" });
    } catch {
      
    }
  }

  async update(request: Request, response: Response, next: NextFunction){
    return response.status(200).json({ message: "updated!" });
  }

  async delete(request: Request, response: Response, next: NextFunction){
    return response.status(200).json({ message: "deleted!" });
  }
}