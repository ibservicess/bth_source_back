import { Request, Response, NextFunction } from 'express';
import database from '../database/connection';
import { sendResetPassword } from '../middlewares/emailHandler';
import { forgotReset, forgotSolicit, forgotValidate } from '../repository/ForgotRepository';
import { transporter } from '../service/emailer';
import { generateRandomString } from '../utils/genUtils';

/**
 * Controller for Forgot Password
 * @category Controllers
 */
export default class ForgotController {

  /**
   * Returns if this token is valid for reset password.
   * @param request 
   * @param response 
   */
  async validate(request: Request, response: Response) {
    try {
      const { token } = request.body;

      await forgotValidate(token).then(isValid => {

        if (isValid) {
          return response.status(200).json({ validate: isValid });
        }

        return response.status(406).json({ validate: isValid });
      });

    } catch (error) {
      return response.status(500).json({ message: "error!" });
    }

  }

  /**
   * Get token and new password, if token is exists and not used the password is changed for new password.
   * @param request 
   * @param response 
   */
  async reset(request: Request, response: Response) {
    const { token, password } = request.body;

    try {
      await forgotReset(token, password);

      return response.status(200).json({ message: "reseted!" });
    } catch (error) {
      return response.status(500).json({ message: "error" });
    }

  }

  /**
   * Two possibilities for solicit a reset password, with email or username.
   * If one of both is true, will send an email for this user.
   * @param request 
   * @param response 
   */
  async solicit(request: Request, response: Response) {
    try {
      const { email, username } = request.body;

      const userData = await forgotSolicit(username, email);

      sendResetPassword(
        userData.email,
        userData.username,
        userData.token,
        userData.name
      );

      return response.status(200).json({ message: "sended!" });
    } catch {
      return response.json({ message: "ops!" });
    }
  }
}