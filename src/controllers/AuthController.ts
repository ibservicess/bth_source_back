import { Request, Response, NextFunction } from 'express';
import { compareHash } from '../utils/hashUtils';

import database from '../database/connection';
import { sign } from 'jsonwebtoken';
import { signJWT, verifyJWT } from '../utils/jwtUtils';

import cookie from 'cookie';
import { logHandler } from '../middlewares/logHandler';

/**
 * Controller for Authentication
 * @category Controllers
 */
export default class AuthController {
  async index(request: Request, response: Response) {

  }

  /**
   * Create a new JWT and Cookie for User authenticated.
   * @param request
   * @param response 
   * @category POST
   */
  async create(request: Request, response: Response) {
    try {
      const { username, password } = request.body;

      const query = await database('users').where({ username });

      const compare = await compareHash(password, query[0]['password']);

      // If passward's hash is valid
      if (!compare) {
        return response.status(401).json({ message: 'Login not accepted' });
      }

      // Create a new Token
      const data = {
        id: query[0]['id'],
        name: query[0]['name'],
        email: query[0]['email']
      }
      const jwt = signJWT(data);

      // Create a new Cookie with JWT
      response.setHeader('Set-Cookie', cookie.serialize('auth', jwt, {
        httpOnly: true,
        secure: process.env.NODE_ENV !== 'development',
        sameSite: 'strict',
        maxAge: 3600,
        path: '/'
      }));

      await logHandler(request, response);

      return response.status(202).json({ token: jwt });

    } catch (error) {
      return response.status(500).json({ message: "Unexpected error while try login" })
    }
  }

  async update(request: Request, response: Response) {
    return response.status(200).json({ message: "updated!" });
  }

  async delete(request: Request, response: Response) {
    return response.status(200).json({ message: "deleted!" });
  }

  /**
   * Verify if a token is valid for access.
   * @param request 
   * @param response
   * @category POST
   */
  async verify(request: Request, response: Response) {
    try {
      const token = request.cookies.auth;
      await verifyJWT(token);

      return response.status(202).json({ message: "hi!" });
    } catch {

      return response.status(401).json({ message: "Not valid!" });
    }

  }
}