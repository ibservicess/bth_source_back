import { Request, Response } from 'express';
import { Client } from "@googlemaps/google-maps-services-js";

export default class GeoController {
  async unique(request: Request, response: Response) {
    const { place_id } = request.query;

    const client = new Client({});

    try {
      const params = {
        place_id: place_id.toString(),
        key: process.env.GOOGLE_MAPS_API
      }
      
      const street = await client.placeDetails({ params });
      
      const details = street.data.result;

      return response.json(details);

    } catch (error) {
      console.log("Error with maps...", error);
      return response.status(500).json({ error: "We have a problem..." }); 
    }
  }

  async routes(request: Request, response: Response) {
    const { origin, destination } = request.query;

    const brute = []
    const points = [];

    const client = new Client({});

    try {
      const params = {
        origin: origin.toString(),
        destination: destination.toString(),
        key: process.env.GOOGLE_MAPS_API
      }

      const routes = await client.directions({ params });

      const steps = routes.data.routes[0].legs[0].steps;
      brute.push(steps);
      steps.map(step => {
        points.push(step.start_location);
        points.push(step.end_location);
      })

      return response.json({ brute, points });
    } catch (e) {
      console.log("Error with maps...", e);
      return response.status(500).json({ error: "We have a problem..." });
    }
  }
}