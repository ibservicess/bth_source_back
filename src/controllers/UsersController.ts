import { Request, Response } from 'express';

import { sendUsername } from '../middlewares/emailHandler';
import { userCreate, userIndex } from '../repository/UsersRepository';
import { createHash } from '../utils/hashUtils';
import { createUsername } from '../utils/userUtils';

/**
 * Controller for Users
 * @category Controllers
 */
export default class UsersController {
  /**
   * Get a list of users.
   * `id` for search specific user.
   * `page` for pagination.
   * @param request 
   * @param response 
   * @category GET
   */
  async index(request: Request, response: Response) {
    try {
      const { id, page = 1 } = request.query;

      const results = await userIndex(Number(page), Number(id));

      response.header('X-Total-Count', String(results.countFixed));
      response.header('X-Total-Pages', String(results.pages));

      return response.json(results.data);
    } catch (error) {
      return response.status(400).json({
        error: "Unexpected error while searching an user(s)"
      });
    }
  }

  /**
   * Create an user.
   * @param request 
   * @param response 
   * @category POST
   */
  async create(request: Request, response: Response) {
    const {
      name,
      last_name,
      company_name,
      cnpj,
      email,
      zip_code,
      address_name,
      address_number,
    } = request.body;

    const password = await createHash(request.body.password);
    const username = await createUsername(email, last_name);

    try {
      const userData = {
        name,
        last_name,
        company_name,
        cnpj,
        email,
        zip_code,
        address_name,
        address_number,
        password,
        username
      }

      await userCreate(userData);

      sendUsername(email, username, `${name} ${last_name}`);

      return response.status(201).send({ username });
    } catch {
      return response.status(400).json({
        error: "Unexpected error while creating new user"
      });

    }

  }

  async update(request: Request, response: Response) {
    return response.status(200).json({ message: "updated!" });
  }

  async delete(request: Request, response: Response) {
    return response.status(200).json({ message: "deleted!" });
  }
}