import { Request, Response, NextFunction } from 'express';
import database from '../database/connection';

/**
 * Controller for Logs
 * @category Controllers
 */
export default class LogController{

  /**
   * Get a list of user's logins.
   * `id` for search specific user.
   * `page` for pagination.
   * @param request 
   * @param response 
   * @category GET
   */
  async login(request: Request, response: Response){
    try{
      const { id, page = 1} = request.query;

      const offset = ((Number(page) - 1) * 100);

      const query = database('access_logs')
        .select('access_logs.id', 
          'users.name', 
          'users.last_name', 
          'access_logs.logged_at')
        .innerJoin('users', 'access_logs.user_id', 'users.id')
        .limit(100)
        .offset(offset);

      if(id){
        const log = await query.where('users.id', Number(id));
        return response.json(log);
      }
      const countObject = database('access_logs').count();
      
      const [ count ]: Array<any> = await countObject;
      const countFixed = Number( count['count(*)'] );
      const pages = Math.round( countFixed / 100 ) + 1;     
      
      response.header('X-Total-Count', String(countFixed));
      response.header('X-Total-Pages', String(pages));
      const results = await query;
      
      return response.json(results);
    } catch(error) {
      return response.status(400).json({
        error: "Unexpected error while searching an user(s)"
      });
    }
  }

  async booking(request: Request, response: Response, next: NextFunction){
    return response.status(200).json({ message: "created!" });
  }
}