import express from 'express';

import TestController from './controllers/TestController';
import UsersController from './controllers/UsersController';
import AuthController from './controllers/AuthController';
import ForgotController from './controllers/ForgotController';
import BookController from './controllers/BookController';
import GeoController from './controllers/GeoController';
import LogController from './controllers/LogController';

import { authHandler } from './middlewares/authHandler';

const routes = express.Router();

const testControllers = new TestController();
const usersControllers = new UsersController();
const authControllers = new AuthController();
const forgotControllers = new ForgotController();
const bookController = new BookController();
const geoControllers = new GeoController();
const logControllers = new LogController();

routes.post('/test', authHandler, testControllers.create)
      .post('/test/email', testControllers.email);

/* USERS */
routes.get('/users', usersControllers.index);
routes.post('/users', usersControllers.create);
routes.put('/users/:id', authHandler, usersControllers.update);
routes.delete('/users/:id', authHandler, usersControllers.delete);

/* AUTH */
routes.post('/auth/login', authControllers.create);
routes.post('/auth/verify', authControllers.verify);

/* FORGOT */
routes.post('/forgot/validate', forgotControllers.validate);
routes.post('/forgot/reset', forgotControllers.reset);
routes.post('/forgot/solicit', forgotControllers.solicit);

/* BOOKING */
routes.get('/books', bookController.index);
routes.post('/books', bookController.create);

/* GEO */
routes.get('/geo/unique', geoControllers.unique);
routes.get('/geo/routes', geoControllers.routes);

/* LOGS */
routes.get('/logs/logins', logControllers.login);

export default routes;