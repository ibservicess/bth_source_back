import { compare, hash } from "bcrypt"

/**
 * Returns a new Hash based with a password.
 * @param password A password to hash.
 * @returns A Promise with a string the processed hash.
 * @category Utils
 */
const createHash = async (password: string): Promise<string> => {
  return await hash(password, 14);
}

/**
 * Compare `Password` and `Hash`.
 * @param password A password for compare with Hash.
 * @param hash Hash with compare password.
 * @returns True if both is the same.
 * @category Utils
 */
const compareHash = async (password: string, hash: string): Promise<boolean> => {
  return await compare(password, hash);
}

export { createHash, compareHash };