import { sign, verify } from "jsonwebtoken";

interface ISignJwt{
  id: number
  name: string
  email: string
}

/**
 * Create a new JWT with `Payload` and `Expiration`.
 * @param data All information for put in JWT
 * @returns A string of JWT.
 * @category Utils
 */
const signJWT = (data: ISignJwt): string => {
  const claims = {
    sub: data.id,
    name: data.name,
    email: data.email
  }

  return sign(claims, process.env.GUID, { expiresIn: '1h' })
}

/**
 * Verify if JWT is valid.
 * The `secret` is get with Env.
 * @param token String of JWT.
 * @returns A `string` of payload or `object` with payload.
 * @category Utils
 */
const verifyJWT = (token: string): string | object => {
  return verify(token, process.env.GUID);
}

export { signJWT, verifyJWT };