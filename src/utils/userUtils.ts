import { generateRandomString } from "./genUtils";

/**
 * Returns a new username generation by `email` and `last name` of user.
 * @param email
 * @param lastName 
 * @returns A string of username
 * @category Utils
 */
const createUsername = (email: string, lastName: string) : string => {  
  // name = name.substr(0, Math.random() * (name.length - 3) + 3);
  const newlastName = lastName.split(' ');
  const newEmail = email.split('@');

  return (newEmail[0] + '_' + generateRandomString(4) + '_' + newlastName[0]).toLowerCase().trim();
}

export { createUsername };