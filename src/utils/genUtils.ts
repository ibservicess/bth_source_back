/**
 * Generate a new random string alphanumeric
 * @param length Length of generate string
 * @returns A new random string
 * @category Utils
 */
export const generateRandomString = (length?: number): string => {
  return Math.random().toString(36).substr(2, length || 100);
}