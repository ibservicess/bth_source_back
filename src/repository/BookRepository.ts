import database from "../database/connection";

/**
 * Book's Interface
 * @property `user_id` Id from User.
 * @property `from` Address from.
 * @property `to` Address to.
 * @property `haulage` Four types of Haulage, 0, 1, 2 and 3.
 * @property `equipament` Two types of Equipament between 0 and 1.
 * @property `id` Book's id
 */
interface IBook {
  user_id: number;
  from: string;
  to: string;
  haulage: number;
  equipament: number;
  id?: number;
}

interface IBookIndex {
  data: IBook[];
  countFixed;
  pages;
}

/**
 * Get a list from database.
 * @param offset Offset for pagination.
 * @param id ID references an user.
 * @category Repository
 */
export const bookIndex = async (page: number, id?: number, userId?: number): Promise<IBookIndex> => {
  const offset = ((page - 1) * 20);

  const query = database('bookings').limit(20).offset(offset);
  const countObject = database('bookings').count();

  if (id) {
    query.where({ id });
    countObject.where({ id });
  }

  if (userId) {
    query.where({ userId });
    countObject.where({ userId });
  }

  const [count]: Array<any> = await countObject;
  const countFixed = Number(count['count(*)']);
  const pages = Math.round(countFixed / 20) + 1;

  const results = await query;

  return {
    data: results,
    countFixed,
    pages
  }
}

/**
 * Create booking in database
 * @param bookData 
 * @category Repository
 */
export const bookCreate = async (bookData: IBook): Promise<void> => {
  const trx = await database.transaction();
  try {
    await trx('bookings').insert({
      from: bookData.from,
      to: bookData.to,
      haulage: bookData.haulage,
      equipament: bookData.equipament,
      user_id: bookData.user_id
    });

    await trx.commit();
  } catch {
    await trx.rollback();
  }
}