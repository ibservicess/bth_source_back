import database from "../database/connection";
import { generateRandomString } from "../utils/genUtils";
import { createHash } from "../utils/hashUtils";

interface IForgotSolicitData {
  name: string;
  username: string;
  token: string;
  user_id: number;
  email: string;
}

/**
 * Verify if token is valid and not was used.
 * @param token Token for validate.
 * @category Repository
 */
export const forgotValidate = async (token: string): Promise<boolean> => {
  const query = database('forgot_passwords');

  try {
    const isValid = await query.where({ token });

    if (isValid[0]['was_used']) {
      return false;
    }

  } catch (error) {
    return false;
  }

  return true;
}

/**
 * Reset the `password` of user and delete the `token` and change `was_used` for true.
 * @param token Token of reset.
 * @param password New password from user.
 */
export const forgotReset = async (token: string, password: string): Promise<void> => {
  const isValid = await forgotValidate(token);

  if (isValid) {
    const newPassword = await createHash(password);

    const query = await database('forgot_passwords').where({ token });

    const trx = await database.transaction();

    try {
      await trx('users')
        .where({
          id: query[0]['user_id']
        })
        .update({ password: newPassword });

      await trx('forgot_passwords')
        .where({ token })
        .update({
          token: null,
          was_used: true
        });

      await trx.commit();

    } catch (error) {
      await trx.rollback();
      console.log("[TRANSACTION][ERROR]", error);
    }
  }
}

/**
 * Search if `username` or `email` is exists and register a token in `forgot_passwords` table.
 * @param username Username of user.
 * @param email Email of user.
 * @category Repository
 */
export const forgotSolicit = async (username: string, email?: string): Promise<IForgotSolicitData> => {
  const query = database('users');
  const data = {
    name: "",
    username: "",
    token: generateRandomString().toLocaleLowerCase(),
    user_id: 0,
    email: ""
  }

  try {

    if (username) {
      const result = await query.where({ username });
      data.name = result[0]['name'];
      data.username = result[0]['username'];
      data.user_id = result[0]['id'];
      data.email = result[0]['email'];
    }

    if (email) {
      const result = await query.where({ email });
      data.name = result[0]['name'];
      data.username = result[0]['username'];
      data.user_id = result[0]['id'];
      data.email = result[0]['email'];
    }

    const trx = await database.transaction();
    try {
      await trx('forgot_passwords').insert({
        token: data.token,
        user_id: data.user_id
      });

      await trx.commit();

    } catch (error) {
      await trx.rollback();
      console.log("[TRANSACTION][ERROR]", error);
    }

    return data;
  } catch (error) {
    console.log("[FORGOT_REPOSITORY][SOLICIT][ERROR]", error);
  }
}