import database from "../database/connection";

interface IUser {
  name: string;
  last_name: string;
  username: string;
  company_name: string;
  cnpj: string;
  email: string;
  zip_code: string;
  address_name: string;
  address_number: string;
  password?: string;
}

interface IUserIndex{
  data: IUser[];
  countFixed;
  pages;
}

/**
 * Get a list from database.
 * @param offset Offset for pagination.
 * @param id ID references an user.
 * @category Repository
 */
export const userIndex = async (page: number, id?: number): Promise<IUserIndex> => {
  const offset = ((page - 1) * 20);

  const query = database('users').limit(20).offset(offset);
  const countObject = database('users').count();

  if (id) {
    query.where({ id });
    countObject.where({ id });
  }

  const [count]: Array<any> = await countObject;
  const countFixed = Number(count['count(*)']);
  const pages = Math.round(countFixed / 20) + 1;

  const results = await query;

  return {
    data: results,
    countFixed,
    pages
  }
}

/**
 * Create user in database
 * @param userData 
 * @category Repository
 */
export const userCreate = async (userData: IUser): Promise<void> => {
  const trx = await database.transaction();
  try {
    const usersIds = await trx('users').insert({
      name: userData.name,
      last_name: userData.last_name,

      company_name: userData.company_name,
      username: userData.username,

      cnpj: userData.cnpj,
      email: userData.email,

      zip_code: userData.zip_code,
      address_name: userData.address_name,

      address_number: userData.address_number,
      password: userData.password
    });

    const user_id = usersIds[0];

    await trx('access_logs').insert({ user_id });

    await trx.commit();
  } catch {
    await trx.rollback();
  }
}